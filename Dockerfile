FROM node:20-alpine
LABEL user="calculadora_propina"
WORKDIR /calculadora_propina/
COPY public/ /calculadora_propina/public
COPY src/ /calculadora_propina/src
COPY package.json /calculadora_propina/
CMD ["npm", "start"]